import { TitaniumOctopusPage } from './app.po';

describe('titanium-octopus App', function() {
  let page: TitaniumOctopusPage;

  beforeEach(() => {
    page = new TitaniumOctopusPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
