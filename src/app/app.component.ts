import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  formShowing: boolean = false;
  title = 'app works!';
  views: Object[] = [
    {
      name: "My Account",
      description: "Edit my account information",
      icon: "account_box"
    },
    {
      name: "My Local Internet",
      description: "Find our the status of your internet in your local area.",
      icon: "network_check"
    }
  ];

  dogs: Object [] = [
    {
      name: "Jorgie",
      description: "This is a cool dog like Jorgie.asdf alsdkjfha;skdjfna;s djfa;sdfas;fas;ldkfas;dlfa ;sdkgfa;sdg;asdfa;sdifjas dfh;asd"
    },
    {
      name: "Bruce",
      description: "A dog from Nicole, is a dog worth having."
    },
    
  ]

}
